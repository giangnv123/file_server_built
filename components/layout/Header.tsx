import { LogOutIcon, MenuIcon, Upload } from 'lucide-react';
import { signOut } from 'next-auth/react';
import Link from 'next/link';
import Search from '../shared/Search';


const Header = ({ toggleNav }: { toggleNav: () => void }) => {
    return (
        <div className='h-16 sticky top-0 z-50 w-full border-b border-slate-300 shadow-md'>
            <div className='bg-white flex items-center h-full pr-3 md:pr-10'>
                <div className='md:w-[250px] w-[80px] flex justify-center'>
                    <h3 className='text-lg w-full text-left font-semibold pl-5  md:block hidden'>
                        File Server
                    </h3>
                    <button className='btn-circle hover:bg-primary-foreground md:hidden' onClick={toggleNav}>
                        <MenuIcon />
                    </button>

                </div>
                <div className='flex-1 mr-6 max-w-[800px]'>
                    <Search placeholder='Search for photos name and description...' />
                </div>
                <Link className=' hidden md:flex items-center gap-3 py-1 px-2 ml-auto text-xs font-semibold
                text-slate-500 hover:text-slate-600 hover:bg-primary-foreground rounded-md' href={'/files'}>
                    <Upload width={20} height={20} />
                    <span className='md:block hidden'>Upload</span>
                </Link>
                <button onClick={() => signOut()} className='flex items-center gap-2 hover:text-red-500 transition-all ml-6 text-slate-600'>
                    <LogOutIcon width={20} height={20} /> <span className='md:block hidden text-sm font-semibold'>Logout</span>
                </button>
            </div>
        </div>
    )
}

export default Header;