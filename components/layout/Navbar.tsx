'use client'

import { cn } from '@/lib/utils';
import { ImagesIcon, StarIcon, Trash2Icon, Upload } from 'lucide-react';
import Image from 'next/image';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { FC } from 'react';

interface NavbarProps {
    user: IUser;
    showNav: boolean;
    toggleNav: () => void
}

interface IUser {
    name: string;
    image: string;
}

const Navbar: FC<NavbarProps> = ({ user, showNav, toggleNav }) => {

    const pathname = usePathname()

    return (
        <div className={cn('md:w-[80px] lg:w-[250px] bg-white relative flex flex-col z-40  border-slate-200 shadow-sm ',

        )}>
            <div
                className={cn('fixed -left-full bg-white md:left-0 w-[80px] lg:w-[250px] overflow-hidden border-r border-border border-2 h-full top-0 p-0 lg:pr-4 transition-all',
                    { 'left-0': showNav })}>
                <div className='h-16 flex pl-4 items-center gap-3'>
                </div>
                <div className='lg:px-6 flex justify-center items-center gap-4 py-4'>
                    <Image alt='user' src={user.image} width={48} height={48} className='rounded-full border border-slate-300' />
                    <h3 className='lg:block hidden'>{user?.name}</h3>
                </div>
                <ul className='flex flex-col mt-6 gap-2 lg:items-stretch items-center'>
                    {navItem.map(item => (
                        <Link href={item.path} key={item.name}
                            className={cn(`hover:bg-gray-100 text-sm font-semibold text-slate-500 cursor-pointer rounded-full
                         btn-circle 
                         lg:justify-start lg:rounded-r-full lg:rounded-l-none lg:w-full `,
                                { 'bg-primary-foreground text-primary': pathname === item.path },
                            )}>
                            <div className='flex items-center lg:gap-5 py-2 lg:px-6 lg:w-full'>
                                {item.icon}
                                <span className='lg:block hidden'>{item.name}</span>
                            </div>
                        </Link>
                    ))}
                </ul>
            </div>
        </div>
    )
}

const navItem = [
    {
        name: 'Photos',
        path: '/',
        icon: <ImagesIcon />
    },
    {
        name: 'Upload files',
        path: '/files',
        icon: <Upload />
    },
    {
        name: 'Bin',
        path: '/trash',
        icon: <Trash2Icon />
    },
    {
        name: 'Favorite',
        path: '/favorite',
        icon: <StarIcon />
    },

]

export default Navbar;