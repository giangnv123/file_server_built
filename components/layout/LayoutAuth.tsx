"use client"
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { useSession } from 'next-auth/react';
import { FC, ReactNode, useState } from 'react';
import { Toaster } from 'sonner';
import Loading from '../shared/Loading';

import { IUser } from '../props/interface';
import SignInUi from '../signIn/SignInUi';
import Header from './Header';
import Navbar from './Navbar';

const queryClient = new QueryClient()

interface LayoutAuthProps {
    children: ReactNode;
}

const LayoutAuth: FC<LayoutAuthProps> = ({ children }) => {
    const { data: session, status } = useSession();

    const [showNav, setShowNav] = useState<boolean>(false);
    const toggleNav = () => setShowNav(!showNav);

    // Handling the loading state
    if (status === "loading") {
        return (
            <div className='h-screen flex justify-center items-center'>
                <Loading />
            </div>
        )
    }

    // If not authenticated, show the login button
    if (status === "unauthenticated") {
        return (
            <SignInUi />
        );
    }

    return (
        <QueryClientProvider client={queryClient}>
            <Toaster position="top-center" richColors />
            <main className=''>
                <Header toggleNav={toggleNav} />
                <div className='flex'>
                    <Navbar showNav={showNav} toggleNav={toggleNav} user={session?.user as IUser} />
                    <div className='flex-1 min-h-screen p-4 '>
                        {children}
                    </div>
                </div>
            </main>
        </QueryClientProvider>
    );
}

export default LayoutAuth;
