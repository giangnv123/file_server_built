"use client"

import { createContext, useContext } from "react";

export const FileContext = createContext<FileContextProps | undefined>(undefined);

export const GetFileContext = () => {
    const file = useContext(FileContext)
    if (file === undefined) {
        throw new Error("useFileContext must be used within a FileProvider")
    }
    return file
}

export interface FileContextProps {
    content: IFile[];
    asyncAcceptFile: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncDeleteFiles: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncRevertFile: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncEditFile: ({ id, title, description }: { id: number, title: string, description: string }) => Promise<boolean>;
}

export enum Type {
    VIDEO = 'video',
    IMAGE = 'image',
}

export interface IFile {
    id: number;
    path: string;
    name: string;
    description: string;
    title: string;
    status: number;
    type: Type;
    zone: number;
    tag: string;
    dateUpdate: number;
    fileSize: number;
    source: string;
    dateCreate: number;
    userCreatedId: number;
    extension: string;
}