import { API_ACCEPT_FILE, API_DELETE_MULTIPLE, API_FILES, API_REVERT_TO_PENDING } from "@/app/_network/api";
import axios, { AxiosError } from "axios";
import { toast } from "sonner";
import { checkResOkay } from "../shared/utilFunction";

export const updateFile = async ({ id, title, description }: { id: number, title: string, description: string }): Promise<boolean> => {
    try {
        const formData = new FormData();
        formData.append('title', title);
        formData.append('description', description);
        const res = await axios.patch(`${API_FILES}/${id}`, formData);
        if (checkResOkay(res.status)) {
            console.log(' success');
            toast.success('Update file successfully')
        }
        return true

    } catch (error: AxiosError<any> | any) {
        toast.error('Fail to update file')
        return false
    }
};

export const approveFile = async ({ idField }: { idField: number[] }) => {
    try {
        const formData = new FormData();
        formData.append('fileId', `${idField}`);
        const res = await axios.post(`${API_ACCEPT_FILE}`, formData);
        if (checkResOkay(res.status)) {
            toast.success(`Move ${idField.length} files to public successfully!`)
            return
        }
    } catch (error: AxiosError<any> | any) {
        console.log(error.response.data);

        toast.error('Fail to move file to public')
    }
};

export const revertToPending = async ({ idField }: { idField: number[] }) => {
    // toast.error("Not implemented yet")
    try {
        const formData = new FormData();
        formData.append('fileId', `${idField}`);
        const res = await axios.post(`${API_REVERT_TO_PENDING}`, formData);
        if (checkResOkay(res.status)) {
            toast.success(`Move ${idField.length} files to pending successfully!`)
            return
        }
    } catch (error: AxiosError<any> | any) {
        console.log(error.response.data);

        toast.error(`Fail to move file to pending`)
    }
};

export const deleteFiles = async ({ idField }: { idField: number[] }) => {
    try {
        const formData = new FormData();
        formData.append('items[]', `${idField}`);

        const res = await axios({
            method: 'delete',
            url: `${API_DELETE_MULTIPLE}`,
            data: formData,
            headers: { 'Content-Type': 'multipart/form-data' },
        });

        if (checkResOkay(res.status)) {
            toast.success(`Delete ${idField.length} files successfully!`);
            return;
        }
    } catch (error) {
        if (axios.isAxiosError(error)) {
            // If the error is an AxiosError, log the response data
            console.log(error.response?.data);
        } else {
            // Handle non-Axios errors
            console.log(error);
        }

        toast.error('Fail to delete files!');
    }
};

