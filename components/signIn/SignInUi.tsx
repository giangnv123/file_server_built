
import { signIn } from 'next-auth/react';
import Image from 'next/image';
import { FC } from 'react';

interface SignInUiProps {

}

const SignInUi: FC<SignInUiProps> = ({ }) => {
    return (
        <div className='w-full h-screen flex justify-center items-center'>
            <button onClick={() => signIn('google')} className='p-4 shadow-md rounded-md gap-4 flex justify-center items-center border'>
                <Image width={48} height={48} alt='google' src={"/google.png"} />
                <h2>Login with Google</h2>
            </button>
        </div>
    )
}

export default SignInUi;