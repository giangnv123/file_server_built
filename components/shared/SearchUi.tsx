'use client'

import { SearchIcon, XIcon } from "lucide-react";
import { FC, memo, useState } from "react";
import { useDebouncedCallback } from "use-debounce";
import { IFile } from "../context/FileContext";

interface SearchProps {
    placeholder?: string,
    files: IFile[],
    setFiles: (files: IFile[]) => void

}

const SearchUi: FC<SearchProps> = ({ placeholder, files, setFiles }) => {
    const [searchValue, setSearchValue] = useState('')
    // Function to normalize Vietnamese (or any Unicode) strings
    const normalizeText = (text: string) => {
        return text.normalize("NFD").replace(/[\u0300-\u036f]/g, "").toLowerCase();
    };

    const handleSearch = useDebouncedCallback((value: string) => {
        const normalizedSearchValue = normalizeText(value);

        // Filter files based on the normalized search value
        const filteredFiles = files.filter(file =>
            normalizeText(file.name).includes(normalizedSearchValue) ||
            normalizeText(file.description).includes(normalizedSearchValue)
        );

        setFiles(filteredFiles);
    }, 300);



    return (
        <div className=" relative text-slate-700 w-full mr-6">
            {searchValue && (
                <button onClick={() => {
                    setSearchValue('')
                    handleSearch('')
                }}
                    className="absolute top-1/2 -translate-y-1/2 right-4 w-10  h-10 rounded-full flex
                justify-center items-center hover:shadow-md hover:bg-gray-200">
                    <XIcon />
                </button>
            )}

            <div className="absolute top-1/2 -translate-y-1/2 left-4 ">
                <SearchIcon className="text-gray-400" />
            </div>
            <input id="search"
                value={searchValue}
                onChange={(e) => {
                    setSearchValue(e.target.value)
                    handleSearch(e.target.value)
                }}
                className="py-2 rounded-md bg-gray-100 px-6 pl-12 placeholder:text-slate-400 placeholder:font-semibold w-full 
                focus:outline-none focus:shadow-md border placeholder:text-sm"
                placeholder={placeholder}
            />
        </div>
    )
}

export default memo(SearchUi);