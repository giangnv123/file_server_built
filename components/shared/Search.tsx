'use client'

import { SearchIcon, XIcon } from "lucide-react";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { FC, memo, useEffect, useState } from "react";
import { useDebouncedCallback } from "use-debounce";

interface SearchProps {
    placeholder: string,
}

const Search: FC<SearchProps> = ({ placeholder }) => {
    const [searchValue, setSearchValue] = useState('')
    const searchParams = useSearchParams()
    const pathname = usePathname()
    const { replace } = useRouter()
    const params = new URLSearchParams(searchParams.toString())

    const searchParamValue = params.get('search') || '';

    const handleSearch = useDebouncedCallback((value: string) => {
        if (value.length > 0 && value.length < 3) return
        params.delete('page')
        // params.set('page', '1')
        value ? params.set('search', value) : params.delete('search')
        replace(`${pathname}?${params.toString()}`)
    }, 300)

    useEffect(() => {
        if (searchParamValue)
            setSearchValue(searchParamValue);
        else (
            setSearchValue(''))

    }, [searchParamValue])

    return (
        <div className=" relative text-slate-700 w-full mr-6">
            {searchValue && (
                <button onClick={() => {
                    setSearchValue('')
                    handleSearch('')
                }}
                    className="absolute top-1/2 -translate-y-1/2 right-4 w-10  h-10 rounded-full flex
                justify-center items-center hover:shadow-md hover:bg-gray-200">
                    <XIcon />
                </button>
            )}

            <div className="absolute top-1/2 -translate-y-1/2 left-4 ">
                <SearchIcon className="text-gray-400" />
            </div>
            <input id="search"
                value={searchValue}
                onChange={(e) => {
                    setSearchValue(e.target.value)
                    handleSearch(e.target.value)
                }}
                className="py-2 rounded-md bg-gray-100 px-6 pl-12 placeholder:text-slate-400 placeholder:font-semibold w-full 
                focus:outline-none focus:shadow-md border placeholder:text-sm"
                placeholder={placeholder}
            />
        </div>
    )
}

export default memo(Search);