import {
    Tooltip,
    TooltipContent,
    TooltipProvider,
    TooltipTrigger,
} from "@/components/ui/tooltip";
import { cn } from "@/lib/utils";
import { ReactNode, memo } from "react";

interface IToolTipUtilProps {
    content: string;
    trigger: ReactNode;
}

function ToolTipUtil({ content, trigger }: IToolTipUtilProps) {
    return (
        <TooltipProvider delayDuration={100}>
            <Tooltip >
                <TooltipTrigger className={cn("")}
                    asChild>
                    {trigger}
                </TooltipTrigger>
                <TooltipContent className="bg-black border-black text-white">
                    <p>{content}</p>
                </TooltipContent>
            </Tooltip>
        </TooltipProvider>
    )
}

export default memo(ToolTipUtil);