export const checkResOkay = (resStatus: number) => {
    return resStatus >= 200 && resStatus <= 300
}

export function formatDate(dateNumber: number): string {
    const date = new Date(dateNumber);

    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0'); // +1 because getMonth() returns 0-11
    const year = date.getFullYear();

    return `${day}-${month}-${year}`;
}
export const invalidPhoneNumber = (phone: string) => !phone.match(/^0[0-9]{9,10}$/)

export const InvalidFullName = (fullname: string) => {
    return (!fullname.trim() || fullname.trim().split(' ').length < 2)
}

export const InvalidEmail = (email: string) => !email.match(/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/)

export function capitalizeFirstLetterAfterSpace(input: string) {
    return input.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
}

export const ErrorText = ({ message }: { message: string }) => {
    return message ? <div className="text-red-500 text-sm">{message}</div> : null;
};

export const toTitleCase = (str: string) => {
    return str.replace(/\w\S*/g, (txt) => {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
};

export const formatDateDetail = (dateString: string) => {
    const date = new Date(dateString);
    const day = date.getDate().toString().padStart(2, '0');
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const hours = date.getHours();
    const minutes = date.getMinutes().toString().padStart(2, '0');
    const ampm = hours >= 12 ? 'pm' : 'am';
    const formattedHour = hours % 12 || 12; // Converts 24h to 12h format and treats 0 as 12

    return `${day}/${month} - ${formattedHour}:${minutes} ${ampm}`;
}


export function convertFileSize(sizeInBytes: number): string {
    const sizeInKB = sizeInBytes / 1024;

    if (sizeInKB > 1000) {
        const sizeInMB = sizeInKB / 1024;
        return `${sizeInMB.toFixed(2)} MB`;
    } else {
        return `${sizeInKB.toFixed(2)} KB`;
    }
}