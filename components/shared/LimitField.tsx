"use client"

import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { useEffect, useState } from 'react'

import {
    Select,
    SelectContent,
    SelectGroup,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"

const LimitField = () => {
    // State is now managed as a string
    const [selectValue, setSelectValue] = useState<string>('10')
    const searchParams = useSearchParams()
    const pathname = usePathname()
    const { replace } = useRouter()
    const params = new URLSearchParams(searchParams.toString())

    const handleChangeLimit = (value: string) => {
        params.set('page', '1')
        params.set('limit', value)
        replace(`${pathname}?${params.toString()}`)
        setSelectValue(value) // Update state with the string value directly
    }

    useEffect(() => {
        const limit = searchParams.get('limit')
        if (limit) {
            // Ensure the value is stored as a string in the state
            setSelectValue(limit)
        }
    }, [searchParams])

    return (
        <Select onValueChange={handleChangeLimit} value={selectValue}>
            <SelectTrigger id='limit' className='max-w-[150px] input-default shadow-sm outline-none border-2 border-gray-300 rounded-none focus:outline-none
            '>
                <SelectValue />
            </SelectTrigger>
            <SelectContent id='limit'>
                <SelectGroup>
                    <SelectItem value="10">Show 10</SelectItem>
                    <SelectItem value="20">Show 20</SelectItem>
                    <SelectItem value="50">Show 50</SelectItem>
                </SelectGroup>
            </SelectContent>
        </Select>
    )
}

export default LimitField;
