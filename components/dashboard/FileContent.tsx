import { Upload } from 'lucide-react';
import { useState } from 'react';
import { GetFileContext, IFile } from '../context/FileContext';
import FileBar from './FileBar';
import FileRendering from './FileRendering/FileRendering';
import AllFilters from './filter/AllFilters';
import QuickUploadDialog from './popup/QuickUploadDialog';

const FileContent = ({ total }: { total: number }) => {

    const fileContext = GetFileContext();

    const { content, asyncAcceptFile, asyncDeleteFiles, asyncRevertFile } = fileContext

    const [checkFiles, setCheckFiles] = useState([] as IFile[]);

    const handleClickCheckFile = (file: IFile) => {

        setCheckFiles(prev => prev.includes(file) ?
            prev.filter(f => f !== file) : [...prev, file])
    }
    return (
        <div>
            <div>
                <div className='flex justify-between mb-3'>
                    <h3 className='font-semibold mb-2'>Filter</h3>
                    <QuickUploadDialog
                        content={content} >
                        <button className='flex items-center  text-gray-500 font-semibold text-sm p-2 rounded-sm border border-gray-300'>
                            <Upload width={20} height={20} />
                            <span className='ml-2'>Quick Upload</span>
                        </button>
                    </QuickUploadDialog>
                </div>
                <div className='flex items-center gap-8 flex-wrap'>
                    <AllFilters />
                </div>
            </div>
            {checkFiles.length > 0 && (
                <FileBar
                    asyncDeleteFiles={asyncDeleteFiles}
                    asyncAcceptFile={asyncAcceptFile}
                    setCheckFiles={setCheckFiles}
                    asyncRevertFile={asyncRevertFile}
                    checkFiles={checkFiles} />
            )}

            {total > 0 ? <p className='text-sm text-gray-500 mt-5 mb-3 font-semibold'>Total: {total} files</p> :
                <p className='text-sm text-gray-500 mt-5 mb-3 font-semibold'>No files found. Please try a different filter</p>}
            <FileRendering
                asyncAcceptFile={asyncAcceptFile}
                asyncRevertFile={asyncRevertFile}
                checkFiles={checkFiles}
                handleClickCheckFile={handleClickCheckFile}
                content={content} />

        </div>

    )
}

export default FileContent;