"use client"

import { CheckIcon, User2Icon, Users2Icon } from 'lucide-react'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { memo, useEffect, useState } from 'react'


export enum Status {
    All = 'all',
    Pending = '0',
    Public = '1',
}

const SelectStatus = () => {

    // State is now managed as a string
    const [selectValue, setSelectValue] = useState<string>("all")
    const searchParams = useSearchParams()
    const pathname = usePathname()
    const { replace } = useRouter()
    const params = new URLSearchParams(searchParams.toString())

    const status = searchParams.get('status')

    const handleChangeStatus = (value: string) => {
        const oppositeStatus = value === Status.Pending ? Status.Public : Status.Pending
        // params.set('page', '1')
        params.delete('page')
        if (selectValue === Status.All) {
            params.set('status', oppositeStatus)
            setSelectValue(oppositeStatus)
        } else if (selectValue === value && selectValue !== Status.All) {
            return
        } else if (selectValue !== Status.All && selectValue !== value) {
            params.delete('status')
            setSelectValue(Status.All)
        }

        replace(`${pathname}?${params.toString()}`)
    }

    useEffect(() => {
        setSelectValue(status ? status : Status.All)
    }, [status])

    return (

        <div className='flex items-center gap-4'>
            <button className='flex items-center gap-3' onClick={() => handleChangeStatus(Status.Pending)}>
                <div className='w-8 h-8 border border-gray-300 flex justify-center items-center'>
                    {(selectValue === Status.Pending || selectValue === Status.All) && <CheckIcon className='text-green-500' />}
                </div>
                <User2Icon />
            </button>

            <button className='flex items-center gap-3' onClick={() => handleChangeStatus(Status.Public)}>
                <div className='w-8 h-8 border border-gray-300 flex justify-center items-center'>
                    {(selectValue === Status.Public || selectValue === Status.All) && <CheckIcon className='text-green-500' />}
                </div>
                <Users2Icon />
            </button>
        </div>

    )
}

export default memo(SelectStatus);
