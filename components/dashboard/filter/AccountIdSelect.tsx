"use client"

import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { memo, useEffect, useState } from 'react'

import { API_GET_ACCOUNT_ID } from '@/app/_network/api'
import {
    Select,
    SelectContent,
    SelectGroup,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"
import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { User2, } from 'lucide-react'

const AccountIdSelect = () => {

    const { data } = useQuery({
        queryKey: ['accounts'],
        queryFn: async (): Promise<string[] | null> => {
            try {
                const res = await axios.get(API_GET_ACCOUNT_ID);
                return res.data;
            } catch (error) {
                console.log(error, ' ERROR');
                return null;
            }
        },
    })

    // State is now managed as a string
    const [selectValue, setSelectValue] = useState<string>("all")
    const searchParams = useSearchParams()
    const pathname = usePathname()
    const { replace } = useRouter()
    const params = new URLSearchParams(searchParams.toString())
    const account = searchParams.get('account')

    const handleChangeAccount = (value: string) => {
        // params.set('page', '1')
        params.delete('page')
        params.set('account', value)
        if (value === 'all') {
            params.delete('account')
        }
        setSelectValue(value) // Update state with the string value directly
        replace(`${pathname}?${params.toString()}`)
    }

    useEffect(() => {
        if (account)
            setSelectValue(account)
        else (
            setSelectValue('all'))

    }, [account])

    if (!data || !data.length) return null;

    return (
        <Select onValueChange={handleChangeAccount} value={selectValue}>
            <SelectTrigger id='account' className='max-w-[160px] input-default shadow-sm outline-none border-2
             border-gray-300 focus:outline-none rounded-sm '>
                <SelectValue />
            </SelectTrigger>
            <SelectContent id='account'>
                <SelectGroup>
                    <SelectItem className='cursor-pointer' value={'all'}>
                        <span className='text-gray-500 flex items-center gap-2'> <User2 /> All Accounts</span>
                    </SelectItem>
                    {data.map(accountId =>
                        <SelectItem key={accountId} className='cursor-pointer' value={String(accountId)}>
                            <span className='text-gray-500 flex items-center gap-2'> <User2 /> {String(accountId)}</span>
                        </SelectItem>)}
                </SelectGroup>
            </SelectContent>
        </Select>
    )
}

export default memo(AccountIdSelect);
