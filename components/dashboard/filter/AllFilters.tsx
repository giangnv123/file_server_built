import { FC } from 'react';

import { memo } from 'react';
import AccountIdSelect from './AccountIdSelect';
import CleanFilter from './CleanFilter';
import SelectStatus from './SelectStatus';
import DateSort from './SortField';
import TypeField from './TypeField';

interface AllFiltersProps {
}

const AllFilters: FC<AllFiltersProps> = ({ }) => {
    return (
        <>
            <SelectStatus />
            <TypeField />
            <DateSort />
            <AccountIdSelect />
            <CleanFilter />
        </>
    )
}

export default memo(AllFilters);