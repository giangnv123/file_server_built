"use client"

import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { memo, useEffect, useState } from 'react'

import {
    Select,
    SelectContent,
    SelectGroup,
    SelectItem,
    SelectTrigger,
    SelectValue,
} from "@/components/ui/select"
import { FileTextIcon, ImagesIcon, VideoIcon } from 'lucide-react'

enum Type {
    Docs = 'doc',
    Image = 'image',
    Video = 'video',
}

const TypeField = () => {

    // State is now managed as a string
    const [selectValue, setSelectValue] = useState<string>(Type.Image)
    const searchParams = useSearchParams()
    const pathname = usePathname()
    const { replace } = useRouter()
    const params = new URLSearchParams(searchParams.toString())
    const type = searchParams.get('type')

    const handleChangeType = (value: string) => {
        // params.set('page', '1')
        params.delete('page')
        params.set('type', value)
        if (value === Type.Image) {
            params.delete('type')
        }
        replace(`${pathname}?${params.toString()}`)
        setSelectValue(value) // Update state with the string value directly
    }

    useEffect(() => {
        if (type)
            setSelectValue(type)
        else
            setSelectValue(Type.Image)

    }, [type])

    return (
        <Select onValueChange={handleChangeType} value={selectValue}>
            <SelectTrigger id='type' className='max-w-[160px] input-default shadow-sm outline-none border-2
             border-gray-300 focus:outline-none rounded-sm
            '>
                <SelectValue className='capitalize' />
            </SelectTrigger>
            <SelectContent id='type'>
                <SelectGroup >
                    <SelectItem className='cursor-pointer' value={Type.Image}>
                        <span className='text-gray-500 flex items-center gap-2  capitalize'> <ImagesIcon /> {Type.Image}</span>
                    </SelectItem>
                    <SelectItem className='cursor-pointer' value={Type.Video}>
                        <span className='text-gray-500 flex items-center gap-2 capitalize'> <VideoIcon /> {Type.Video}</span>
                    </SelectItem>
                    <SelectItem className='cursor-pointer' value={Type.Docs}>
                        <span className='text-gray-500 flex items-center gap-2 capitalize'> <FileTextIcon /> {Type.Docs}</span>
                    </SelectItem>
                </SelectGroup>
            </SelectContent>
        </Select>
    )
}

export default memo(TypeField);
