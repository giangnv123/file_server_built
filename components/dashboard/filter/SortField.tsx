"use client"

import { Calendar, ChevronDown } from 'lucide-react'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { memo, useEffect, useState } from 'react'

enum SortOrder {
    Asc = 'asc',
    Desc = 'desc',
}

const DateSort = () => {

    // State is now managed as a string
    const [orderByAsc, setOrderByAsc] = useState<boolean>(true)
    const searchParams = useSearchParams()
    const pathname = usePathname()
    const { replace } = useRouter()
    const params = new URLSearchParams(searchParams.toString())
    const type = searchParams.get('sort')

    const toggleOrder = () => {
        if (orderByAsc) {
            params.set('sort', SortOrder.Desc)
        } else {
            params.delete('sort')
        }

        replace(`${pathname}?${params.toString()}`)
    }

    useEffect(() => {
        if (type === "desc")
            setOrderByAsc(false)
        else
            setOrderByAsc(true)
    }, [type])

    return (
        <button onClick={toggleOrder}
            className='flex items-center gap-3 cursor-pointer text-gray-500 p-2 rounded-sm border-2 border-gray-300'>
            <Calendar /> Sort By Date <ChevronDown className={orderByAsc ? 'rotate-180  transition-all' : ' transition-all'} />
        </button>
    )
}

export default memo(DateSort);
