"use client"

import { CircleX } from 'lucide-react'
import { usePathname, useRouter, useSearchParams } from 'next/navigation'
import { memo } from 'react'

const CleanFilter = () => {

    const searchParams = useSearchParams()
    const pathname = usePathname()
    const { replace } = useRouter()
    const params = new URLSearchParams(searchParams.toString())

    const handleCleanFilter = () => {
        replace(pathname, undefined);
    }

    if (searchParams.toString() === "") return null

    return (
        <button className='btn-filter flex  items-center gap-2 py-1.5 px-2  text-gray-400' onClick={handleCleanFilter}>
            <CircleX /> Clean filter
        </button>
    )
}

export default memo(CleanFilter);
