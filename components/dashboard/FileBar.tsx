import { cn } from '@/lib/utils';
import { Trash2Icon, User2Icon, Users2Icon, XIcon } from 'lucide-react';
import { FC, memo } from 'react';
import { IFile } from '../context/FileContext';
import ToolTipUtil from '../shared/ToolTipUtil';

interface FileBarProps {
    setCheckFiles: any;
    checkFiles: IFile[];
    asyncAcceptFile: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncDeleteFiles: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncRevertFile: ({ idField }: { idField: number[] }) => Promise<void>;
}
const FileBar: FC<FileBarProps> = ({ checkFiles, setCheckFiles, asyncAcceptFile, asyncDeleteFiles, asyncRevertFile }) => {
    console.log("FILEBAR RENDER");

    const idField = checkFiles.map(f => f.id)
    return (
        <div className={cn('h-16 fixed top-0 left-0 w-full transition-all px-6 py-2 flex items-center bg-white z-[999] opacity-0',
            { 'opacity-100': checkFiles.length > 0 })}>
            <div className='flex items-center justify-between w-full'>
                <div className='flex items-center gap-6'>
                    <button className='cursor-pointer w-8 h-8 rounded-full hover:bg-gray-100 flex justify-center items-center'
                        onClick={() => setCheckFiles([])}>
                        <XIcon />
                    </button>
                    <p className='text-slate-600'>
                        {checkFiles.length} selected
                    </p>
                </div>
                <div className='flex items-center gap-4'>
                    <ToolTipUtil
                        content='Move All To Pending'
                        trigger={
                            <button
                                onClick={() => {
                                    asyncRevertFile({ idField })
                                    setCheckFiles([])
                                }}
                                className='btn-circle'>
                                <User2Icon className='text-blue-500' />
                            </button>
                        } />
                    <ToolTipUtil
                        content='Approve all'
                        trigger={
                            <button onClick={() => {
                                asyncAcceptFile({ idField })
                                setCheckFiles([])
                            }} className='btn-circle'>
                                <Users2Icon className='text-blue-500' />
                            </button>
                        } />
                    <ToolTipUtil
                        content='Delete all'
                        trigger={
                            <button
                                onClick={() => {
                                    asyncDeleteFiles({ idField })
                                    setCheckFiles([])
                                }} className='btn-circle'>
                                <Trash2Icon className='text-blue-500' />
                            </button>
                        } />
                </div>

            </div>
        </div>
    )
}

export default memo(FileBar);