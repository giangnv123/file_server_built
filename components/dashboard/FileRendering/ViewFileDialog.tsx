'use client'
import { domain } from "@/app/_network/api"
import {
    Dialog,
    DialogContent,
    DialogTrigger
} from "@/components/ui/dialog"
import { ReactNode, memo } from "react"
import { IFile } from "../../context/FileContext"

const ViewFileDialog = ({ file, children }: { file: IFile, children: ReactNode }) => {
    return (
        <Dialog>
            <DialogTrigger asChild>
                {children}
            </DialogTrigger>
            <DialogContent className=" w-full max-w-7xl h-[95vh]">
                <iframe width={"100%"} height={"100%"} src={`${domain}/xapi/file/files/${file.name}`}>
                </iframe>
            </DialogContent>
        </Dialog>
    )
}

export default memo(ViewFileDialog);