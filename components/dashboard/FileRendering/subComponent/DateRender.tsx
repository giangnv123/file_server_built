import { formatDate } from '@/components/shared/utilFunction';
import { cn } from '@/lib/utils';
import { Calendar } from 'lucide-react';
import { FC, memo } from 'react';



interface DateRender {
    date: number;
    icon?: JSX.Element
}

const DateRender: FC<DateRender> = ({ date, icon = <Calendar width={15} height={15} /> }) => {
    return (
        <p className={cn('flex items-start gap-2 text-xs sm:text-sm  text-gray-400 mb-1',)}>
            {icon}
            <span className='max-w-[80%] text-xs font-semibold'>
                {formatDate(date)}
            </span>
        </p>
    )
}

export default memo(DateRender);