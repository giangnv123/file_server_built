import { cn } from '@/lib/utils';
import { User2Icon, Users2Icon } from 'lucide-react';
import { FC, memo } from 'react';
import { IFile } from '../../context/FileContext';
import ToolTipUtil from '../../shared/ToolTipUtil';

interface ChangePublicPending {
    asyncAcceptFile: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncRevertFile: ({ idField }: { idField: number[] }) => Promise<void>;
    file: IFile
}

const PublicPendingChanging: FC<ChangePublicPending> = ({ asyncAcceptFile, asyncRevertFile, file }) => {

    return (
        <>
            <ToolTipUtil
                trigger={
                    <button
                        onClick={() => asyncAcceptFile({ idField: [file.id] })}
                        disabled={file.status === 1}
                        className='btn-circle-sm bg-gray-100'>
                        <Users2Icon width={18} height={18} className={cn('text-gray-500 hover:text-gray-700',
                            { "text-blue-500 hover:text-blue-500": file.status === 1 })} />
                    </button>
                }
                content='Move to public'
            />
            <ToolTipUtil
                trigger={
                    <button
                        onClick={() => asyncRevertFile({ idField: [file.id] })}
                        disabled={file.status === 0} className='btn-circle-sm bg-gray-100'>
                        <User2Icon width={18} height={18} className={cn('text-gray-500 hover:text-gray-700',
                            { "text-blue-500  hover:text-blue-500": file.status === 0 })} />
                    </button>
                }
                content='Move to pending'
            />
        </>
    )
}

export default memo(PublicPendingChanging);