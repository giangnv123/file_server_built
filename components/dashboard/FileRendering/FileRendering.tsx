'use client';
import { cn } from '@/lib/utils';
import { Bookmark, LayoutGrid, LayoutList, List, NotebookTextIcon } from 'lucide-react';
import { FC, useState } from 'react';
import { IFile } from '../../context/FileContext';
import ContentForm from './ContentForm';
import RenderFileHandler from './RenderFileHandler';
import { default as Calendar } from './subComponent/DateRender';

interface FileRenderingProps {
    content: IFile[];
    checkFiles: IFile[];
    handleClickCheckFile: (file: IFile) => void;
    asyncAcceptFile?: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncRevertFile?: ({ idField }: { idField: number[] }) => Promise<void>;
    uploadMode?: boolean;
}

export enum Grid {
    DETAIL = 'detail',
    GRID = 'grid',
    CARD = 'card',
}

const FileRendering: FC<FileRenderingProps> = ({ content,
    checkFiles,
    asyncAcceptFile,
    asyncRevertFile,
    handleClickCheckFile,
    uploadMode,
}) => {
    const [grid, setGrid] = useState<Grid>(Grid.CARD)

    const detailView = grid === Grid.DETAIL
    const listView = grid === Grid.GRID

    return (
        <div>
            {!uploadMode &&
                <div className='flex items-center justify-end gap-3 text-gray-500'>
                    <LayoutGrid onClick={() => setGrid(Grid.CARD)}
                        className={cn('cursor-pointer opacity-80 hover:opacity-100', { 'text-blue-500': grid === Grid.CARD })} />
                    <LayoutList onClick={() => setGrid(Grid.DETAIL)}
                        className={cn('cursor-pointer opacity-80 hover:opacity-100', { 'text-blue-500': detailView })} />
                    <List onClick={() => setGrid(Grid.GRID)}
                        className={cn('cursor-pointer opacity-80 hover:opacity-100', { 'text-blue-500': grid === Grid.GRID })} />
                </div>}
            <div className={cn('grid lg:grid-cols-3 xl:grid-cols-4 grid-cols-2  gap-x-4 gap-y-6 pb-10 pt-5', {
                'xl:grid-cols-3 grid-cols-2 pt-0': uploadMode,
                'xl:grid-cols-2 md:grid-cols-1 lg:grid-cols-2 grid-cols-1': detailView,
                'grid-cols-1 xl:grid-cols-1 md:grid-cols-1 lg:grid-cols-1': listView
            })}>
                {content?.map(file => (
                    <div className={cn('transition-all rounded-md shadow-sm',
                        { 'p-2.5 md:p-5 bg-blue-50': checkFiles.includes(file) })} key={file.id}>
                        {(!uploadMode &&
                            !checkFiles.includes(file)) &&
                            <Calendar date={file.dateCreate} />}
                        <div className={cn('flex flex-col rounded-md h-full bg-white', {
                            'flex-row gap-4': detailView
                        })}>
                            <div className={cn('', {
                                'flex gap-3 items-center': listView,
                                'max-w-[50%]': detailView,
                            })}>
                                <div className={cn('', {
                                    'max-w-[125px]': listView

                                })}>
                                    <RenderFileHandler
                                        file={file}
                                        checkFiles={checkFiles}
                                        handleClickCheckFile={handleClickCheckFile}
                                        asyncAcceptFile={asyncAcceptFile}
                                        asyncRevertFile={asyncRevertFile}
                                        uploadMode={uploadMode} />


                                </div>
                                {listView &&
                                    <div className='flex flex-col gap-2 text-gray-600'>
                                        <div className='flex text-gray-600 font-semibold pt-1.5 px-1 gap-1.5'>
                                            <Bookmark width={20} height={20} />
                                            <span className='text-sm max-w-[85%]'>{file.title || 'No Title'}</span>
                                        </div>
                                        <div className='flex text-gray-600 font-semibold pt-1.5 px-1 gap-1.5'>
                                            <NotebookTextIcon width={20} height={15} className='text-gray-600' />
                                            <span className='w-[85%]' >
                                                {file.description || 'No description'}
                                            </span>
                                        </div>
                                    </div>
                                }
                            </div>

                            {uploadMode ?
                                <div className='flex text-gray-600 font-semibold pt-1.5 px-1 gap-1.5'>
                                    <Bookmark width={20} height={20} />
                                    <span className='text-sm max-w-[85%]'>{file.title || 'No Title'}</span>
                                </div> :
                                !listView && <ContentForm detailView={detailView} file={file} />
                            }
                        </div>
                    </div>
                ))}
            </div>
        </div>

    )
}

export default FileRendering;