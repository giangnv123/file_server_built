'use client'
import {
    Dialog,
    DialogContent,
    DialogTrigger
} from "@/components/ui/dialog"
import { ReactNode, memo } from "react"

const ImageDialog = ({ children }: { children: ReactNode }) => {
    return (
        <Dialog modal >
            <DialogTrigger asChild>
                {children}
            </DialogTrigger>
            <DialogContent className=" w-full max-w-3xl h-[80vh] flex justify-center shadow-lg  ">
                {children}
            </DialogContent>
        </Dialog>
    )
}

export default memo(ImageDialog);