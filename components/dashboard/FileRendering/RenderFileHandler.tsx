import { domain } from '@/app/_network/api';
import { cn } from '@/lib/utils';
import { CheckIcon, XIcon } from 'lucide-react';
import Image from 'next/image';
import { FC, memo } from 'react';
import { IFile, Type } from '../../context/FileContext';
import PublicPendingChanging from './ChangePublicPending';
import ImageDialog from './ImageDialog';
import ViewFileDialog from './ViewFileDialog';



interface RenderFileHandler {
    file: IFile;
    checkFiles?: IFile[];
    handleClickCheckFile: (file: IFile) => void;
    asyncAcceptFile?: ({ idField }: { idField: number[] }) => Promise<void>;
    asyncRevertFile?: ({ idField }: { idField: number[] }) => Promise<void>;
    viewMode?: boolean;
    uploadMode?: boolean;
}

export enum Extension {
    Excel = 'xlsx',
    Word = 'docx',
    Pdf = 'pdf',
}

const RenderImageHandle: FC<RenderFileHandler> = ({ file,
    uploadMode,
    handleClickCheckFile,
    asyncRevertFile,
    asyncAcceptFile,
    viewMode,
    checkFiles }) => {


    const getFileName = (file: IFile) => {
        return file.name.lastIndexOf('/') > 0 ? file.name.substring(file.name.lastIndexOf('/') + 1) : file.name
    }

    return (
        <div className='relative cursor-auto group'>
            {file.type === Type.IMAGE ?
                <ImageDialog>
                    <Image
                        draggable={false}
                        className='rounded-md shadow-lg'
                        src={`${domain}/xapi/file/files/${file.name}`} alt={file.name} width={500} height={500} />
                </ImageDialog>
                :
                file.type === Type.VIDEO ?
                    <video className='rounded-md shadow-md' controls width="500" height="500" muted playsInline>
                        <source src={`${domain}/xapi/file/files/${file.name}`} type="video/mp4" />
                        Your browser does not support the video tag.
                    </video>
                    :
                    file.extension === Extension.Excel ?

                        <div className='rounded-md shadow-md p-3'>
                            <Image alt='excel' className='' src='/excel-logo.avif' width={100} height={100} />
                            <p className='text-gray-500'>  {getFileName(file)}</p>
                        </div>
                        :
                        <ViewFileDialog file={file}>
                            <div className='rounded-md shadow-md p-3 cursor-pointer'>
                                <Image alt='pdf' className='' src='/pdf-logo' width={100} height={100} />
                                <p className='text-gray-500'>  {getFileName(file)}</p>
                            </div>
                        </ViewFileDialog>

            }
            <button onClick={() => {
                !uploadMode ? handleClickCheckFile(file) : handleClickCheckFile(file)
            }}
                className={cn('top-3 left-3 absolute w-6 h-6 flex justify-center items-center text-gray-600 bg-white opacity-0 group-hover:opacity-60 transition-all group-hover:hover:opacity-100 rounded-full z-10', {
                    'opacity-100 bg-blue-500 text-white group-hover:opacity-100 -left-2 -top-2': !viewMode && checkFiles && checkFiles.includes(file),
                    'right-2  opacity-100 bg-red-500 text-white  group-hover:opacity-100 transition-all group-hover:hover:opacity-100 ': viewMode
                })}>
                {viewMode ? <XIcon /> : <CheckIcon width={15} height={15} />}
            </button>
            {!uploadMode && <div className='opacity-40 group-hover:opacity-100 flex items-center gap-1 absolute top-3 right-3'>
                {asyncAcceptFile && asyncRevertFile &&
                    <PublicPendingChanging file={file} asyncAcceptFile={asyncAcceptFile} asyncRevertFile={asyncRevertFile} />}
            </div>}
        </div>
    )
}

export default memo(RenderImageHandle);