import { convertFileSize } from '@/components/shared/utilFunction';
import { Bookmark, CheckIcon, CloudUpload, NotebookPenIcon, NotebookTextIcon, User2Icon, XIcon } from 'lucide-react';
import { FC, memo, useCallback, useEffect, useRef, useState } from 'react';
import { toast } from 'sonner';
import { GetFileContext, IFile } from '../../context/FileContext';
import { Textarea } from '../../ui/textarea';
import DateRender from './subComponent/DateRender';

interface ContentFormProps {
    file: IFile
    detailView: boolean
}

const ContentForm: FC<ContentFormProps> = ({ file, detailView }) => {
    const fileContext = GetFileContext();

    const { asyncEditFile } = fileContext;

    const [editMode, setEditMode] = useState<boolean>(false);
    const titleRef = useRef<HTMLTextAreaElement>(null);

    const [formData, setFormData] = useState({ title: file.title, description: file.description });

    const handleChangeInput = useCallback((e: React.ChangeEvent<HTMLTextAreaElement>) => {
        setFormData(prev => ({ ...prev, [e.target.name]: e.target.value }));
    }, []);


    useEffect(() => { // Step 3: Use useEffect to focus on the textarea
        if (editMode) {
            titleRef.current?.focus(); // Automatically focus on the title textarea when edit mode is enabled
        }
    }, [editMode]);

    const handleSubmitForm = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (file.description === formData.description && file.title === formData.title) {
            toast.info('No changes detected. Please modify the fields before saving');
            return
        }
        const editSuccess = await asyncEditFile({ id: file.id, ...formData });

        if (editSuccess) {
            setEditMode(false);
        }
    }

    return (
        <form
            onSubmit={handleSubmitForm} onDoubleClick={() => setEditMode(true)}
            className='flex flex-col gap-2 p-1 pt-2 bg-white relative'>
            {detailView &&
                <div className='flex flex-col gap-2 text-gray-400 text-xs sm:text-sm font-semibold '>
                    <DateRender date={file.dateUpdate} icon={<NotebookPenIcon width={15} height={15} />} />
                    <p className=' flex items-center gap-2'>
                        <User2Icon width={20} height={20} />
                        {file.userCreatedId}
                    </p>
                    <p className='flex items-center gap-2'><CloudUpload /> {convertFileSize(file.fileSize)}</p>
                    <p>Status: {file.status === 1 ? 'Public' : 'Pending'}</p>
                    <p>From: {file.source}</p>
                </div>}
            <h2 className=' text-xs sm:text-base font-semibold flex items-start gap-2 text-gray-500'>
                <Bookmark width={20} height={20} className='text-gray-600' />
                {!editMode ? <span className='w-[85%]'>{file.title || 'No title'}</span> :
                    <Textarea ref={titleRef} value={formData.title} onChange={handleChangeInput} name='title'
                        spellCheck={false} placeholder='Add Title...' className='rounded-sm w-[85%]'>{file.title}</Textarea>}
            </h2>
            <p className='flex items-start gap-2  text-xs sm:text-sm text-gray-600'>
                <NotebookTextIcon width={20} height={15} className='text-gray-600' />
                {!editMode ? <span className='w-[85%]' >
                    {file.description || 'No description'}
                </span> :
                    <Textarea
                        rows={2}
                        value={formData.description} onChange={handleChangeInput} name='description'
                        spellCheck={false} placeholder='Add Description...' className='rounded-sm w-[85%] h-12'>{file.description}</Textarea>}
            </p>
            <div className='flex justify-end'>
            </div>
            {editMode && <div className='flex items-center gap-3 justify-end'>
                <button onClick={() => setEditMode(false)}
                    className='btn-circle-sm text-red-500 hover:bg-red-50'>
                    <XIcon />
                </button>
                <button type='submit' className='btn-circle-sm text-green-500 hover:bg-green-50'>
                    <CheckIcon />
                </button>
            </div>}

        </form>
    )
}

export default memo(ContentForm);