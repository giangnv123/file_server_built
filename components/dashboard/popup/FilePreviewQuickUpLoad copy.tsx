'use client'

import { ScrollArea } from "@/components/ui/scroll-area";
import { FC } from 'react';
import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd';
import { IFile } from '../../context/FileContext';
import RenderFileHandler from '../FileRendering/RenderFileHandler';

interface FilePreviewQuickUpLoadProps {
    fileSelected: IFile[];
    handleClickSelectFile: (file: IFile) => void;
    setFileSelected: (files: IFile[]) => void;
}

const FilePreviewQuickUpLoad: FC<FilePreviewQuickUpLoadProps> = ({ fileSelected, handleClickSelectFile, setFileSelected }) => {
    const onDragEnd = (result: any) => {
        if (!result.destination) return; // Dropped outside the list

        const items = Array.from(fileSelected);
        const [reorderedItem] = items.splice(result.source.index, 1);
        items.splice(result.destination.index, 0, reorderedItem);

        setFileSelected(items); // Update the state with the new order
    };

    return (
        <div className="w-full max-w-[220px] ml-auto">
            <h3 className="font-semibold text-gray-500 mb-2">Files selected: {fileSelected.length}</h3>
            <DragDropContext onDragEnd={onDragEnd}>
                <Droppable droppableId="droppable-files" direction="vertical">
                    {(provided) => (
                        <ScrollArea className="h-[80vh] w-full max-w-[220px] px-3" ref={provided.innerRef} {...provided.droppableProps}>
                            <div className="flex flex-col gap-3">
                                {fileSelected.map((file, index) => (
                                    <Draggable key={file.id} draggableId={String(file.id)} index={index}>
                                        {(provided) => (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.draggableProps}
                                                {...provided.dragHandleProps}
                                                className="flex flex-col">
                                                <RenderFileHandler
                                                    file={file}
                                                    viewMode={true}
                                                    handleClickCheckFile={handleClickSelectFile}
                                                    uploadMode={true}
                                                />
                                            </div>
                                        )}
                                    </Draggable>
                                ))}
                                {provided.placeholder}
                            </div>
                        </ScrollArea>
                    )}
                </Droppable>
            </DragDropContext>
        </div>
    );
};

export default FilePreviewQuickUpLoad;
