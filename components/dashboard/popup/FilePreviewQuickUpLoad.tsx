'use client'

import { ScrollArea } from "@/components/ui/scroll-area";
import { Reorder } from "framer-motion";
import { FC, memo } from 'react';
import { IFile } from '../../context/FileContext';
import RenderFileHandler from "../FileRendering/RenderFileHandler";

interface FilePreviewQuickUpLoadProps {
    fileSelected: IFile[];
    handleClickSelectFile: (file: IFile) => void;
    setFileSelected: (files: IFile[]) => void;
}

const FilePreviewQuickUpLoad: FC<FilePreviewQuickUpLoadProps> = ({ fileSelected, handleClickSelectFile, setFileSelected }) => {

    return (
        <div className="w-full max-w-[220px] ml-auto">
            <h3 className="font-semibold text-gray-500 mb-2 px-3">Files selected: {fileSelected.length}</h3>
            <ScrollArea className="h-[80vh] w-full max-w-[220px] px-3" >
                <div className="flex flex-col gap-3">
                    <Reorder.Group axis="y" values={fileSelected}
                        onReorder={setFileSelected}
                    >
                        {fileSelected.map((file) => (
                            <Reorder.Item className="pb-4" key={file.id} value={file}>
                                <RenderFileHandler
                                    file={file}
                                    viewMode={true}
                                    handleClickCheckFile={handleClickSelectFile}
                                    uploadMode={true}
                                />
                            </Reorder.Item>
                        ))}
                    </Reorder.Group>
                </div>
            </ScrollArea>
        </div>
    );
};

export default memo(FilePreviewQuickUpLoad);
