'use client'
import {
    Dialog,
    DialogContent,
    DialogTrigger
} from "@/components/ui/dialog"
import { ScrollArea } from "@/components/ui/scroll-area"
import { ReactNode, memo, useState } from "react"
import { IFile } from "../../context/FileContext"
import SearchUi from "../../shared/SearchUi"
import FileRendering from "../FileRendering/FileRendering"
import FilePreviewQuickUpLoad from "./FilePreviewQuickUpLoad"


const QuickUploadDialog = ({ children, content }: { children: ReactNode, content: IFile[] }) => {
    const [files, setFiles] = useState<IFile[]>(content)
    const [fileSelected, setFileSelected] = useState<IFile[]>([]);

    const handleClickSelectFile = (file: IFile) => {
        setFileSelected(prev => fileSelected.includes(file) ?
            prev.filter(f => f !== file) : [...prev, file])
    }

    return (
        <Dialog modal>
            <DialogTrigger asChild >
                {children}
            </DialogTrigger>
            <DialogContent className=" w-full max-w-7xl h-[95vh]">
                <div className="flex gap-4">
                    <div className="flex w-1/2 flex-col gap-2">
                        <SearchUi
                            files={content}
                            setFiles={setFiles}
                            placeholder="Search" />
                        <ScrollArea className="max-h-[80vh] w-full rounded-md border">
                            <FileRendering
                                checkFiles={fileSelected}
                                handleClickCheckFile={handleClickSelectFile}
                                uploadMode={true}
                                content={files}
                            />
                        </ScrollArea>
                    </div>
                    <div className="ml-8 max-w-[150px]">
                        <FilePreviewQuickUpLoad
                            setFileSelected={setFileSelected}
                            fileSelected={fileSelected}
                            handleClickSelectFile={handleClickSelectFile} />
                    </div>
                </div>
            </DialogContent>
        </Dialog >
    )
}

export default memo(QuickUploadDialog);