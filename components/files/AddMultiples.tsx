'use client';
import { API_FILES_MULTIPLE } from '@/app/_network/api';
import axios from 'axios';
import { Upload, XIcon } from 'lucide-react';
import { FC, useState } from 'react';
import { toast } from 'sonner';
import { Button } from '../ui/button';
import { Input } from '../ui/input';
import { Label } from '../ui/label';
import { Textarea } from '../ui/textarea';
import PreviewElement from './PreviewElement';

interface content {
    title: string;
    description: string
}

interface AddMultiplesProps { }

const AddMultiples: FC<AddMultiplesProps> = () => {
    const [selectedFiles, setSelectedFiles] = useState<File[]>([]);
    const [content, setContent] = useState<content[]>([])

    const changeContent = (index: number, event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>, name: string) => {
        setContent(prev => {
            const updatedContent = [...prev];
            if (!updatedContent[index]) {
                return [...updatedContent, { title: '', description: '' }]
            }
            return updatedContent.map((c, i) => i === index ? { ...c, [name]: event.target.value } : c);
        })
    }

    const [previewUrls, setPreviewUrls] = useState<string[]>([]);
    const [status, setStatus] = useState('1');
    const [accountId, setAccountId] = useState<number>(0);

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files) {
            const filesArray = Array.from(event.target.files);
            setSelectedFiles(filesArray);
            // Generate preview URLs
            const urls = filesArray.map(file => URL.createObjectURL(file));
            setPreviewUrls(urls);
            for (let i = 0; i < filesArray.length; i++) {
                setContent(prev => [...prev, { title: '', description: '' }])
            }
        }
    };

    const handleRemoveFile = (index: number) => {
        // Remove the file and its preview URL by index
        const newFiles = [...selectedFiles];
        newFiles.splice(index, 1);
        setSelectedFiles(newFiles);

        const newUrls = [...previewUrls];
        newUrls.splice(index, 1);
        setPreviewUrls(newUrls);
        setContent(prev => prev.filter((_, i) => i !== index))
    };

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        const formData = new FormData();

        if (!accountId) {
            toast.error('Please enter an account id.');
            return;
        }

        if (selectedFiles.length === 0) {
            toast.error('Please select at least one file.');
            return;
        }

        selectedFiles.forEach(file => {
            formData.append('files', file);
        });

        const currentDomain = window.location.origin;
        console.log(currentDomain, "Current domain");


        formData.append('status', `${status}`);
        formData.append('test', 'haha cà phê');
        formData.append('account', `${accountId}`);
        formData.append('source', currentDomain)
        const contentArray: content[] = []
        content.slice(0, selectedFiles.length).forEach(c => {
            contentArray.push(c);
        })

        const encodedContent = btoa(unescape(encodeURIComponent(JSON.stringify(contentArray))));

        formData.append('content', encodedContent);

        status === '1' && formData.append('role', 'ADMIN')

        try {
            await axios.post(API_FILES_MULTIPLE, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data; charset=utf-8'
                },
            });
            toast.success('File uploaded successfully.');
            setSelectedFiles([]);
            setPreviewUrls([]);
            setContent([]);
        } catch (error) {
            // Handle upload error
            console.error('Error uploading files:', error);
            toast.error('Failed to upload files. Please try again.');
        }
    };


    return (
        <div className='p-4 border border-gray-200 rounded-md shadow-md max-w-4xl mx-auto my-8 w-full min-h-[300px]'>
            <h4 className='text-lg font-semibold mb-4'>Upload Multiple Files</h4>
            <form onSubmit={handleSubmit} className='space-y-4'>
                {!previewUrls.length &&
                    <label htmlFor='files' className=' w-40 h-24 rounded-md shadow-sm border border-gray-300 flex justify-center items-center cursor-pointer hover:shadow-lg'>
                        <div className='flex items-center justify-center gap-2 text-slate-600 text-sm font-semibold'>
                            <Upload />
                            <h2 className='text-center'> Upload</h2>
                        </div>

                    </label>}
                <input
                    hidden
                    id='files'
                    type="file"
                    multiple
                    onChange={handleFileChange}
                    className='mt-1 p-2 w-full border rounded-md'
                />
                <div className='grid lg:grid-cols-3 sm:grid-cols-2 grid-cols-1 gap-4'>
                    {selectedFiles.map((file, index) => {

                        const isVideo = file.type.startsWith('video')
                        const isImage = file.type.startsWith('image')

                        return (
                            <div className='relative' key={index}>
                                <PreviewElement
                                    key={index}
                                    file={file}
                                    isVideo={isVideo}
                                    isImage={isImage}
                                    previewUrl={previewUrls[index]} />
                                <div className='flex flex-col gap-2 mt-1'>
                                    <div className='flex flex-col gap-1'>
                                        <Label htmlFor={`title${index}`}>Title</Label>
                                        <Input id={`title${index}`} spellCheck={false}
                                            value={content[index]?.title} onChange={(e) => changeContent(index, e, 'title')} />
                                    </div>
                                    <div className='flex flex-col gap-1'>
                                        <Label htmlFor={`des${index}`}>Description</Label>
                                        <Textarea id={`des${index}`} spellCheck={false}
                                            value={content[index]?.description} onChange={(e) => changeContent(index, e, 'description')} className=''></Textarea>
                                    </div>
                                </div>
                                <button
                                    type="button"
                                    onClick={() => handleRemoveFile(index)}
                                    className='w-6 h-6 absolute top-0 right-0 bg-red-500 text-white rounded-full p-1 m-1 flex justify-center items-center'
                                >
                                    <XIcon />
                                </button>
                            </div>
                        )
                    }

                    )}

                </div>
                <div className='flex items-center gap-2'>
                    <label className='text-sm font-medium text-gray-700'>Status:</label>
                    <label className='inline-flex items-center'>
                        <input
                            type="radio"
                            value="0"
                            checked={status === '0'}
                            onChange={(e) => setStatus(e.target.value)}
                            className='text-indigo-600 border-gray-300'
                        />
                        <span className='ml-2'>Pending</span>
                    </label>
                    <label className='inline-flex items-center'>
                        <input
                            type="radio"
                            value="1"
                            checked={status === '1'}
                            onChange={(e) => setStatus(e.target.value)}
                            className='text-indigo-600 border-gray-300'
                        />
                        <span className='ml-2'>Public</span>
                    </label>
                </div>
                <div className='flex gap-3 items-center'>
                    <label htmlFor='account' className='w-32 block'>Account Id</label>
                    <Input
                        id='account'
                        type='number'
                        value={accountId} onChange={(e) => setAccountId(Number(e.target.value))} />
                </div>
                <div className='flex justify-end mt-auto'>
                    <Button type="submit"  >
                        Upload Files
                    </Button>
                </div>
            </form>
        </div>
    );
};
export default AddMultiples;
