import { FileIcon } from 'lucide-react';
import Image from 'next/image';
import { FC, memo } from 'react';

interface PreviewElementProps {
    file: File | undefined | null,
    isVideo?: boolean,
    isImage?: boolean,
    previewUrl?: string
}

const PreviewElement: FC<PreviewElementProps> = ({ file, isVideo, isImage, previewUrl }) => {

    if (!file || !previewUrl) return null;
    return (
        <div >
            {previewUrl && isVideo ? (
                <div>
                    <video controls width="500" height="300" src={previewUrl} className='mb-2 rounded-md' />
                </div>
            ) : previewUrl && isImage ? (
                <div className='relative'>
                    <Image width={500} height={300} src={previewUrl} alt="Preview" className='mb-2 rounded-md shadow-md' />
                </div>
            ) :
                <div>
                    <FileIcon /> {file?.name}
                </div>}
        </div>
    )
}

export default memo(PreviewElement);