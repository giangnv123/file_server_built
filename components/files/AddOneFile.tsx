'use client';
import axios from 'axios';
import { Upload, XIcon } from 'lucide-react';

import { API_FILES } from '@/app/_network/api';
import { FC, useState } from 'react';
import { toast } from 'sonner';
import { checkResOkay } from '../shared/utilFunction';
import { Button } from '../ui/button';
import PreviewElement from './PreviewElement';

const AddOneFile: FC = () => {
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [status, setStatus] = useState('1');
    const [file, setFile] = useState<File | null>(null);
    const [previewUrl, setPreviewUrl] = useState<string | null>(null);

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files ? event.target.files[0] : null;
        setFile(file);

        if (file) {
            const url = URL.createObjectURL(file);
            setPreviewUrl(url);
        } else {
            setPreviewUrl(null);
        }
    };

    const handleRemoveFile = () => {
        setFile(null);
        setPreviewUrl(null);
        URL.revokeObjectURL(previewUrl!); // Revoke the object URL to avoid memory leaks
    };

    const handleSubmitFile = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!file) {
            toast.error('Please select a file.');
            return;
        }

        const formData = new FormData();
        formData.append('title', title);
        formData.append('description', description);
        formData.append('status', status);
        formData.append('file', file);
        formData.append('account', '10');
        formData.append('source', 'chotot.com');

        console.log(formData, '\n DATA BEFORE SENDING REQUEST');


        try {
            const response = await axios.post(API_FILES, formData, { // Placeholder URL
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            });

            if (checkResOkay(response.status)) {
                toast.success('File uploaded successfully.');
                setTitle('');
                setDescription('');
                setFile(null);
                setPreviewUrl(null);
            } else {
                toast.error('Failed to upload file. Please try again.');
            }
        } catch (error) {
            toast.error('Failed to upload file. Please try again.');
            console.error('Error uploading file:', error);
        }
    };

    const isVideo = file?.type.startsWith('video');
    const isImage = file?.type.startsWith('image');

    return (
        <div className='p-4 border border-gray-200 rounded-md shadow-md max-w-4xl mx-auto my-8 w-full'>
            <h4 className='text-xl font-semibold mb-4 capitalize'>Upload one file</h4>
            <form onSubmit={handleSubmitFile} className='space-y-4'>
                <div>
                    <label className='block text-sm font-medium text-gray-700'>Title:</label>
                    <input
                        spellCheck={false}
                        type="text"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                        className='mt-1 p-2 w-full border rounded-md'
                    />
                </div>
                <div>
                    <label className='block text-sm font-medium text-gray-700'>Description:</label>
                    <textarea
                        spellCheck={false}

                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                        className='mt-1 p-2 w-full border rounded-md'
                    />
                </div>
                <div className='flex items-center gap-2'>
                    <label className='text-sm font-medium text-gray-700'>Status:</label>
                    <label className='inline-flex items-center'>
                        <input
                            type="radio"
                            value="0"
                            checked={status === '0'}
                            onChange={(e) => setStatus(e.target.value)}
                            className='text-indigo-600 border-gray-300'
                        />
                        <span className='ml-2'>Pending</span>
                    </label>
                    <label className='inline-flex items-center'>
                        <input
                            type="radio"
                            value="1"
                            checked={status === '1'}
                            onChange={(e) => setStatus(e.target.value)}
                            className='text-indigo-600 border-gray-300'
                        />
                        <span className='ml-2'>Public</span>
                    </label>
                </div>
                <div>
                    {!previewUrl && <label htmlFor='file' className=' w-40 h-24 rounded-md shadow-sm border border-gray-300 flex justify-center items-center cursor-pointer hover:shadow-lg'>
                        <div className='flex items-center justify-center gap-2 text-slate-600 text-sm font-semibold'>
                            <Upload />
                            <h2 className='text-center'> Upload</h2>
                        </div>
                    </label>}
                    <div className='relative max-w-md'>
                        {previewUrl && <button
                            type="button"
                            onClick={handleRemoveFile}
                            className='btn-circle bg-red-500 text-white absolute top-1.5 right-1.5 z-20 cursor-pointer'
                        >
                            <XIcon />
                        </button>}
                        {previewUrl &&
                            <PreviewElement file={file} previewUrl={previewUrl} isVideo={isVideo} isImage={isImage} />}
                    </div>

                    <input
                        id='file'
                        hidden
                        type="file"
                        onChange={handleFileChange}
                        className='mt-1 p-2 w-full border rounded-md'
                    />
                </div>
                <div className='flex justify-end'>
                    <Button type="submit">
                        Upload File
                    </Button>
                </div>
            </form>
        </div>
    );
};


export default AddOneFile;
