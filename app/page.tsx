"use client"
import { FC } from "react"

import { FileContext, Type } from "@/components/context/FileContext"
import FileContent from "@/components/dashboard/FileContent"
import { approveFile, deleteFiles, revertToPending, updateFile } from "@/components/fetch/api_call"
import Loading from "@/components/shared/Loading"
import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query"
import axios from "axios"
import { API_FILES } from "./_network/api"


enum Sort {
  ASC = 'asc',
  DESC = 'desc'
}



interface pageProps {
  searchParams: {
    search: string,
    page: number | string,
    limit: string | number,
    status: number,
    date: string,
    sort: Sort,
    account: string,
    type: Type
  }
}

const Page: FC<pageProps> = ({ searchParams }) => {


  const queryClient = useQueryClient()

  const page = searchParams?.page || 1
  const limit = searchParams?.limit || 100

  const status = (Number(searchParams.status) !== 1 && Number(searchParams.status) !== 0) ? '' : searchParams.status
  const search = searchParams?.search || ''
  const sort = searchParams?.sort || ''
  const account = searchParams?.account && searchParams?.account === 'all' ? '' : searchParams?.account || ''
  const type = searchParams?.type || ''

  const { data, isLoading } = useQuery({
    queryKey: ['files', search, page, limit, status, sort, type, account],
    queryFn: async () => {
      try {
        const res =
          await axios.get(`${API_FILES}?search=${search}&page=${Number(page) - 1}&limit=${limit}&status=${status}&sort=${sort}&type=${type}&account=${account}`);
        return res.data;
      } catch (error) {
        console.log(error, ' ERROR');
      }
    }
  })

  const { mutateAsync: asyncAcceptFile } = useMutation({
    mutationFn: approveFile,
    onSuccess: async () => {
      queryClient.invalidateQueries({ queryKey: ['files'] })
    },
  });

  const { mutateAsync: asyncEditFile } = useMutation({
    mutationFn: updateFile,
    onSuccess: async () => {
      queryClient.invalidateQueries({ queryKey: ['files'] })
    },
  });

  const { mutateAsync: asyncRevertFile } = useMutation({
    mutationFn: revertToPending,
    onSuccess: async () => {
      queryClient.invalidateQueries({ queryKey: ['files'] })
    },
  });

  const { mutateAsync: asyncDeleteFiles } = useMutation({
    mutationFn: deleteFiles,
    onSuccess: async () => {
      queryClient.invalidateQueries({ queryKey: ['files'] })
    },
  });

  console.log(data, ' data');

  if (isLoading) return <div className="p-10 flex justify-center"> <Loading /></div>

  if (!data) return <h3>Something went wrong</h3>

  const content = data.content;

  return (
    <FileContext.Provider value={{ content, asyncAcceptFile, asyncDeleteFiles, asyncRevertFile, asyncEditFile }}>
      <FileContent total={data.numberOfElements} />
    </FileContext.Provider>
  );
}


export default Page