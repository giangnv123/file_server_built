import AddMultiples from '@/components/files/AddMultiples';
import AddOneFile from '@/components/files/AddOneFile';
import { FC } from 'react';

interface pageProps {

}

const page: FC<pageProps> = ({ }) => {
    return (
        <div className=''>
            <AddOneFile />
            <AddMultiples />
        </div>
    )
}

export default page;