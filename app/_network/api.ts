export const domain = `https://drive.xpi.vn`
// export const domain = `http://localhost:8088`

export const API_FILES = `${domain}/xapi/file`;
export const API_FILES_MULTIPLE = `${domain}/xapi/file/multiple`;
export const API_ACCEPT_FILE = `${domain}/xapi/file/admin/accept-file`;
export const API_DELETE_MULTIPLE = `${domain}/xapi/file/multiple`;
export const API_REVERT_TO_PENDING = `${domain}/xapi/file/admin/revert-file`;
export const API_GET_ACCOUNT_ID = `${domain}/xapi/file/accounts`;

